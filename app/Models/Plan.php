<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
    ];

    protected $casts = [
        'price' => 'integer',
    ];

    public function days()
    {
        return $this->belongsToMany(DeliveryDay::class);
    }

    public function getPresentablePriceAttribute()
    {
        return round($this->price / 100, 2);
    }
}
