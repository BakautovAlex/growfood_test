<?php

namespace App\Transformers;

use App\Models\Plan;
use League\Fractal\TransformerAbstract;

class PlanTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'days',
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'days',
    ];

    /**
     * A Fractal transformer.
     *
     * @param Plan $plan
     *
     * @return array
     */
    public function transform(Plan $plan)
    {
        return [
            'id' => $plan->id,
            'name' => $plan->name,
            'price' => $plan->presentable_price,
        ];
    }

    public function includeDays(Plan $plan)
    {
        return $this->collection($plan->days, new DeliveryDayTransformer());
    }
}
