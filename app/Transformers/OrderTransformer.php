<?php

namespace App\Transformers;

use App\Models\Order;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'plan',
        'client',
    ];

    /**
     * A Fractal transformer.
     *
     * @param Order $order
     *
     * @return array
     */
    public function transform(Order $order)
    {
        return [
            'id' => $order->id,
            'first_delivery_day' => $order->first_delivery_day->format('Y-m-d'),
            'contact_name' => $order->contact_name
        ];
    }

    public function includePlan(Order $order)
    {
        return $this->item($order->plan, new PlanTransformer());
    }

    public function includeClient(Order $order)
    {
        return $this->item($order->client, new ClientTransformer());
    }
}
