<?php

namespace App\Transformers;

use App\Models\DeliveryDay;
use League\Fractal\TransformerAbstract;

class DeliveryDayTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @param DeliveryDay $day
     *
     * @return array
     */
    public function transform(DeliveryDay $day)
    {
        return [
            'id' => $day->id,
            'name' => $day->name,
        ];
    }
}
