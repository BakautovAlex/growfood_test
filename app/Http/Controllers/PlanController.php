<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use App\Transformers\PlanTransformer;

class PlanController extends Controller
{
    public function index()
    {
        $plans = Plan::with('days')->get();

        return fractal($plans, new PlanTransformer())->respond();
    }
}
