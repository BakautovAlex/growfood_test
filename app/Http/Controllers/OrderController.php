<?php

namespace App\Http\Controllers;

use App\Http\Requests\Order\StoreRequest;
use App\Models\Client;
use App\Models\Order;
use App\Transformers\OrderTransformer;

class OrderController extends Controller
{
    public function index()
    {
        return fractal(Order::with(['plan', 'client', 'plan.days'])->get(), new OrderTransformer())
            ->parseIncludes(['plan', 'client'])
            ->respond();
    }

    public function store(StoreRequest $request)
    {
        $client = Client::firstOrCreate(['phone' => $request->phone], [
            'name' => $request->name,
        ]);

        $order = $client->orders()->create([
            'plan_id' => $request->plan_id,
            'first_delivery_day' => $request->first_delivery_day,
            'contact_name' => $request->name,
        ]);

        return fractal($order, new OrderTransformer())
            ->parseIncludes(['plan', 'client'])
            ->respond();
    }
}
