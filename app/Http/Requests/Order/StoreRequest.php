<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'phone' => 'required|size:10|regex:/^[0-9]+$/',
            'plan_id' => 'required|exists:plans,id',
            'first_delivery_day' => 'required|date_format:Y-m-d|after:tomorrow',
            'address' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Необходимо указать имя',
            'name.max' => 'Слишком длинное значение (255 - максимальная длина)',
            'phone.required' => 'Необходимо указать номер телефона',
            'phone.size' => 'Длина номера - 10 цифр, код страны указывать не нужно',
            'phone.regex' => 'Номер должен состоять только из цифр',
            'first_delivery_day.required' => 'Необходимо выбрать первый день доставки',
            'first_delivery_day.date_format' => 'Дата должна быть в формате ГГГГ-ММ-ДД',
            'first_delivery_day.after' => 'Ближайшая дата доставки - завтрашний день',
            'address.required' => 'Необходимо указать адрес доставки',
        ];
    }
}
