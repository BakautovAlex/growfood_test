<?php

use App\Http\Controllers\OrderController;
use App\Http\Controllers\PlanController;
use Illuminate\Support\Facades\Route;

Route::apiResource('order', OrderController::class);
Route::apiResource('plan', PlanController::class);
