<?php

namespace Database\Seeders;

use App\Models\DeliveryDay;
use App\Models\Plan;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (!DeliveryDay::count()) {
            $this->call(DeliveryDaySeeder::class);
        }

        if (!Plan::count()) {
            $this->call(PlanSeeder::class);
        }
    }
}
