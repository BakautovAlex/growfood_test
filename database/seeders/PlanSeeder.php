<?php

namespace Database\Seeders;

use App\Models\Plan;
use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Plan::create([
            'name' => 'Базовый',
            'price' => 100000,
        ])
            ->days()
            ->sync([1, 3, 5]);

        Plan::create([
            'name' => 'Продвинутый',
            'price' => 300000,
        ])
            ->days()
            ->sync([1, 2, 3, 4, 5]);

        Plan::create([
            'name' => 'Максимальный',
            'price' => 600000,
        ])
            ->days()
            ->sync(range(1, 7));
    }
}
