<?php

namespace Database\Seeders;

use App\Models\DeliveryDay;
use Illuminate\Database\Seeder;

class DeliveryDaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DeliveryDay::insert([
            [
                'name' => 'Понедельник',
            ],
            [
                'name' => 'Вторник',
            ],
            [
                'name' => 'Среда',
            ],
            [
                'name' => 'Четверг',
            ],
            [
                'name' => 'Пятница',
            ],
            [
                'name' => 'Суббота',
            ],
            [
                'name' => 'Воскресенье',
            ],
        ]);
    }
}
