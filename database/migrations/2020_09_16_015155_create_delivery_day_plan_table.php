<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryDayPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_day_plan', function (Blueprint $table) {
            $table->id();
            $table->foreignId('delivery_day_id')->constrained()->onDelete('no action');
            $table->foreignId('plan_id')->constrained()->cascadeOnDelete();

            $table->index('delivery_day_id');
            $table->index('plan_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_day_plan');
    }
}
